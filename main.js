async function register ({
  registerHook,
}) {
  registerHook({
    target: 'filter:api.videos.list.params',
    handler: (params) => ({
      ...params,
      sort: params.sort === '-publishedAt' ?
        '-originallyPublishedAt' :
        params.sort === 'publishedAt' ?
        'originallyPublishedAt' :
        params.sort,
    }),
  });

  const setPublishedAt = video => {
    if (!video) return video

    return Object.assign(video, {
      publishedAt: video.originallyPublishedAt || video.publishedAt,
    });
  }

  registerHook({
    target: 'filter:api.video.get.result',
    handler: setPublishedAt,
  });

  registerHook({
    target: 'filter:api.videos.list.result',
    handler: ({ data, total }) => ({
      data: data.map(setPublishedAt),
      total,
    }),
  });

  registerHook({
    target: 'filter:api.video-playlist.videos.list.result',
    handler: ({ data, total }) => ({
      data: data.map(setPublishedAt),
      total,
    }),
  });
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
