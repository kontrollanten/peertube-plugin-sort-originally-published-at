# peertube-plugin-sort-originally-published-at

Plugin to sort *Recently added* videos by *originally published at* date,
it will also display the originally published date as date in the video view.
